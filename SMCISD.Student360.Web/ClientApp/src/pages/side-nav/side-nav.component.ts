import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from '../../app/services/api/api.service';
import { AccessLevelDefinition, People } from '../../app/services/api/current-user.service';
import { AuthenticationService, Token } from '../../app/services/authentication/authentication.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  access: boolean;
  tokenSubscription: Subscription;
  token: Token;
  people: People;
  constructor(
    private router: Router,
    public apiService: ApiService,
    private auth: AuthenticationService,
  ) {
    this.people = new People;
    }

  ngOnInit(): void {
    this.apiService.currentUser.getProfile().subscribe(
      result => {
        this.people = result;
      }
    );

    this.tokenSubscription = this.auth.tokenInfo.subscribe(result => {
      this.token = result;
      this.access = parseInt(this.token.level_id) == 0 ? true : false; 
    });
  }


}
