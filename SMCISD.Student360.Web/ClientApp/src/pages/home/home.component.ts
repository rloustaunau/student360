import { Component } from '@angular/core';
import { ApiService } from '../../app/services/api/api.service';
import { AccessToSystem, CurrentUserApiService, People } from '../../app/services/api/current-user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
    people: People;
  constructor(private apiService: ApiService,
    private currentUserService: CurrentUserApiService, private access: AccessToSystem) {
    this.people = new People;
    this.apiService.currentUser.getProfile().subscribe(
      result => {
        this.people = result;

        this.access = new AccessToSystem();
        this.access.email = this.people.electronicMailAddress;
        this.access.fullName = this.people.firstName + ' ' + this.people.lastSurname;

        this.currentUserService.accesToday(this.access.email).subscribe(accessToday => {
          if (!accessToday) {
            this.currentUserService.createAccess(this.access).subscribe(data => {

            });
          }
        });

          

        
      }
    );
  }
}
