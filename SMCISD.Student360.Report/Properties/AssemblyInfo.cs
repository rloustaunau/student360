﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// La información general acerca de un ensamblado se controla a través del siguiente 
// conjunto de atributos. Cambie los valores de estos atributos para modificar la información 
// asociada a un ensamblado.
[assembly: AssemblyTitle("SMCISD.Student360.Report")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("SMCISD.Student360.Report")]
[assembly: AssemblyCopyright("Copyright ©  2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Al establecer ComVisible en false, se consigue que los tipos de este ensamblado no sean visibles 
// para los componentes COM. Si tiene que acceder a un tipo en este ensamblado desde 
// COM, establezca el atributo ComVisible en true en ese tipo.
[assembly: ComVisible(false)]

// El siguiente GUID es para el ID. del typelib, si este proyecto se expone en COM
[assembly: Guid("c77074d6-0f6d-4e55-b141-4a5362e1ab75")]

// La información de versión de un ensamblado consta de los cuatro valores siguientes:
//
//      Versión principal
//      Versión secundaria
//      Número de compilación
//      Revisión
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
